#!/usr/bin/env python

from time import time

import matplotlib.pyplot as plt
import numpy as np

from bayesnet import DiscreteRandomVariable
import bayesnet.pmf
import bayesnet.opencl as cl


def main():

    nb_variables_to_test = range(5, 29)
    nb_repetitions = 20

    precomputed_times = []
    opencl_times = []
    for nb_variables in nb_variables_to_test:

        variables = [DiscreteRandomVariable('a') for _ in range(nb_variables)]
        left_variables = variables[:int(np.ceil(nb_variables / 2))]
        right_variables = variables[int(np.ceil(nb_variables / 2)):]

        # Precomputed products
        left = bayesnet.pmf.ProbabilityMassFunction(left_variables)
        right = bayesnet.pmf.ProbabilityMassFunction(right_variables)

        precomputed_time = 0
        product = bayesnet.pmf.ProbabilityMassFunctionProduct(left, right, 30)
        for _ in range(nb_repetitions):
            start = time()
            product.update()
            stop = time()
            precomputed_time += stop - start

        precomputed_times.append(precomputed_time / nb_repetitions)

        # OpenCL support
        left = cl.ProbabilityMassFunction(left_variables)
        right = cl.ProbabilityMassFunction(right_variables)

        opencl_time = 0
        product = cl.ProbabilityMassFunctionProduct(left, right, 30)
        for _ in range(nb_repetitions):
            start = time()
            product.update()
            stop = time()
            opencl_time += stop - start

        opencl_times.append(opencl_time / nb_repetitions)

    plt.figure(figsize=(5, 5), dpi=150)
    plt.plot(nb_variables_to_test, precomputed_times, label='Precomputed')
    plt.plot(nb_variables_to_test, opencl_times, label='OpenCL')
    plt.title('Products of probability mass functions')
    plt.xlabel('Number of variables')
    plt.ylabel('Average time over {} repetitions'.format(nb_repetitions))
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
