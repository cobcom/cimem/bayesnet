import unittest
from itertools import chain

import numpy as np
from recur.abc import ancestors, descendants

import bayesnet.opencl as bcl
from bayesnet.core import DiscreteRandomVariable
from bayesnet.base import ProbabilityMassFunction
from bayesnet.junction import construct_junction_trees, moral_graphs
from bayesnet.junction import JunctionTreeNode
from bayesnet.junction import _collect, _distribute, _marginals
from bayesnet.junction import Marginals
from bayesnet.testing import ProbabilityMassFunctionMarginal
from bayesnet.testing import ProbabilityMassFunctionProduct


class TestCollect(unittest.TestCase):
    """Test the bayesnet.junction._collect function"""

    def test_linear_junction_tree(self):
        """Test using a simple linear junction tree"""

        # Build a junction tree directly.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        root_pmf = ProbabilityMassFunction([a, b])
        root = JunctionTreeNode({a, b}, set(), [root_pmf])
        child_pmf = ProbabilityMassFunction([b, c])
        child = JunctionTreeNode({b, c}, {b}, [child_pmf])
        root.add(child)

        update_list = _collect(root)

        # The update list contains a single table.
        self.assertEqual(len(update_list), 1)

        # The root has nothing to collect.
        self.assertIsNone(root.collect)

        # Child child should have a single new table.
        self.assertIsNotNone(child.collect)
        self.assertEqual(child.collect, update_list[0])


class TestDistribute(unittest.TestCase):
    """Test the bayesnet.junction._distribute function"""

    def test_linear_junction_tree(self):
        """Test using a simple linear junction tree"""

        # Build a junction tree directly.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        root_pmf = ProbabilityMassFunction([a, b])
        root = JunctionTreeNode({a, b}, set(), [root_pmf])
        child_pmf = ProbabilityMassFunction([b, c])
        child = JunctionTreeNode({b, c}, {b}, [child_pmf])
        root.add(child)

        update_list = _collect(root)
        update_list += _distribute(root)

        # We have 2 tables to update.
        self.assertEqual(len(update_list), 2)

        # The root node should have nothing to distribute.
        self.assertIsNone(root.distribute)

        # The child should have received a table.
        self.assertIsNotNone(child.distribute)
        self.assertEqual(child.distribute, update_list[-1])


class TestMarginals(unittest.TestCase):
    """Test the bayesnet.junction._marginals function"""

    def test_linear_junction_tree(self):
        """Test using a simple linear junction tree"""

        # Build a junction tree directly.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        root_pmf = ProbabilityMassFunction([a, b], [0.1, 0.2, 0.3, 0.4])
        root = JunctionTreeNode({a, b}, set(), [root_pmf])
        child_pmf = ProbabilityMassFunction([b, c], [0.4, 0.3, 0.2, 0.1])
        child = JunctionTreeNode({b, c}, {b}, [child_pmf])
        root.add(child)

        update_list = _collect(root)
        update_list += _distribute(root)
        marginals, marginals_update_list = _marginals(root)
        update_list += marginals_update_list

        # There should be 3 marginals, one for each variable.
        self.assertEqual(len(marginals), 3)
        for marginal in marginals:
            self.assertEqual(len(marginal.variables), 1)
        variables = [m.variables[0] for m in marginals]
        self.assertEqual(marginals[variables.index(a)],
                         ProbabilityMassFunction([a], [0.13/0.46, 0.33/0.46]))
        self.assertEqual(marginals[variables.index(b)],
                         ProbabilityMassFunction([b], [0.28/0.46, 0.18/0.46]))
        self.assertEqual(marginals[variables.index(c)],
                         ProbabilityMassFunction([c], [0.28/0.46, 0.18/0.46]))


class TestMarginalsClass(unittest.TestCase):
    """Test the bayesnet.junction.Marginals class"""

    def test_product_marginals_class(self):
        """Test computing marginals with other product and marginal classes"""

        # Build a simple Bayesian network.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        # Compute the marginals with different implementations of products and
        # marginals.
        root_pmf = ProbabilityMassFunction([a, b], [0.1, 0.2, 0.3, 0.4])
        child_pmf = ProbabilityMassFunction([b, c], [0.4, 0.3, 0.2, 0.1])
        marginals = Marginals(
            [root_pmf, child_pmf],
            ProbabilityMassFunctionProduct,
            ProbabilityMassFunctionMarginal)

        root_pmf = bcl.ProbabilityMassFunction([a, b], [0.1, 0.2, 0.3, 0.4])
        child_pmf = bcl.ProbabilityMassFunction([b, c], [0.4, 0.3, 0.2, 0.1])
        cl_marginals = Marginals(
            [root_pmf, child_pmf],
            bcl.ProbabilityMassFunctionProduct,
            bcl.ProbabilityMassFunctionMarginal)

        # The marginals should be implementation independent.
        for variable in [a, b, c]:
            self.assertEqual(marginals[variable], cl_marginals[variable])

    def test_update(self):
        """Test the update method"""

        # Build a simple Bayesian network.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        root_pmf = ProbabilityMassFunction([a, b], [0.1, 0.2, 0.3, 0.4])
        child_pmf = ProbabilityMassFunction([b, c], [0.4, 0.3, 0.2, 0.1])

        # Compute the marginals.
        marginals = Marginals([root_pmf, child_pmf])

        self.assertEqual(marginals[a],
                         ProbabilityMassFunction([a], [0.13/0.46, 0.33/0.46]))
        self.assertEqual(marginals[b],
                         ProbabilityMassFunction([b], [0.28/0.46, 0.18/0.46]))
        self.assertEqual(marginals[c],
                         ProbabilityMassFunction([c], [0.28/0.46, 0.18/0.46]))

        # Modify the input PMFs.
        root_pmf.probabilities = np.array([0.4, 0.3, 0.2, 0.1])

        # Update the marginals.
        marginals.update()

        self.assertEqual(marginals[a],
                         ProbabilityMassFunction([a], [0.37/0.54, 0.17/0.54]))
        self.assertEqual(marginals[b],
                         ProbabilityMassFunction([b], [0.42/0.54, 0.12/0.54]))
        self.assertEqual(marginals[c],
                         ProbabilityMassFunction([c], [0.32/0.54, 0.22/0.54]))


class TestMoralGraphs(unittest.TestCase):
    """Test the bayesnet.junction.moral_graphs function"""

    def test_simplest_case(self):
        """Test building the simplest moral graph"""

        # Build a test PMF with two parents sharing 1 child.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        pmf = ProbabilityMassFunction([a, b, c])

        # Compute the moral graph.
        graphs = moral_graphs([pmf])

        # There should only be 1 graph and it should contain all the nodes
        # with links between all.
        self.assertEqual(len(graphs), 1)
        nodes = [n for n in graphs[0][1]]
        self.assertEqual(len(nodes), 3)
        for node in nodes:
            neighbors = node.neighbors
            self.assertEqual(len(neighbors), 2)
            self.assertSetEqual(set(neighbors), set(nodes) - {node})


class TestJunctionTrees(unittest.TestCase):
    """Test the bayesnet.junction.junction_trees function"""

    def test_simplest_case(self):
        """Test building the junction tree on a single PMF"""

        # Build a test PMF with two parents sharing 1 child.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        pmf = ProbabilityMassFunction([a, b])

        # Compute the moral graph.
        trees = construct_junction_trees([pmf])

        # There should only be 1 junction tree with a single node.
        self.assertEqual(len(trees), 1)
        self.assertEqual(len([n for n in ancestors(trees[0])]), 1)
        self.assertEqual(len([n for n in descendants(trees[0])]), 1)

        # All the variables should be in the single node.
        node = [n for n in trees[0]][0]
        self.assertSetEqual(node.variables, {a, b})

        # The boundary is empty.
        self.assertSetEqual(node.boundary, set())

        # The is only one table.
        self.assertListEqual(node.tables, [pmf])

    def test_diamond(self):
        """Test building the junction tree with a diamond shaped graph"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        d = DiscreteRandomVariable('d')

        tables = [
            ProbabilityMassFunction([a, b]),
            ProbabilityMassFunction([a, c]),
            ProbabilityMassFunction([b, d]),
            ProbabilityMassFunction([c, d]),
        ]

        junction_trees = construct_junction_trees(tables)

        # There should be a single junction tree.
        self.assertEqual(len(junction_trees), 1)
        junction_tree = junction_trees[0]

        # The junction tree has 2 nodes.
        self.assertEqual(len([n for n in junction_tree]), 2)

        # The head of the tree should contain 3 variables and the boundary 0.
        self.assertEqual(len(junction_tree.variables), 3)
        self.assertEqual(len(junction_tree.boundary), 0)
        self.assertEqual(len(junction_tree.tables), 2)

        # The tables should contain only variables that are in the node.
        domains = [t.variables for t in junction_tree.tables]
        self.assertTrue(set(chain(*domains)) == junction_tree.variables)

        # The subtree should contain 3 variables and the boundary 2.
        sub_junction_tree = list(n for n in junction_tree)[1]
        self.assertEqual(len(sub_junction_tree.variables), 3)
        self.assertEqual(len(sub_junction_tree.boundary), 2)
        self.assertEqual(len(sub_junction_tree.tables), 2)

        # The tables should contain only variables that are in the node.
        domains = [t.variables for t in sub_junction_tree.tables]
        self.assertTrue(set(chain(*domains)) == sub_junction_tree.variables)

    def test_split_trees(self):
        """Test building the junction trees with disconnected PMFs"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        d = DiscreteRandomVariable('d')

        left = ProbabilityMassFunction([a, b])
        right = ProbabilityMassFunction([c, d])

        junction_trees = construct_junction_trees([left, right])

        # There should be two trees.
        self.assertEqual(len(junction_trees), 2)

        junction_tree = junction_trees[0]
        self.assertEqual(len([n for n in descendants(junction_tree)]), 1)
        self.assertEqual(len([n for n in ancestors(junction_tree)]), 1)

        junction_tree = junction_trees[1]
        self.assertEqual(len([n for n in descendants(junction_tree)]), 1)
        self.assertEqual(len([n for n in ancestors(junction_tree)]), 1)
