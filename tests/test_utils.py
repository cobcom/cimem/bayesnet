from unittest import TestCase

import numpy as np

from bayesnet import DiscreteRandomVariable
from bayesnet.utils import reorder_probabilities


class TestReorderProbabilities(TestCase):
    """Test the bayesnet.utils.reorder_probabilities"""

    def test_simple_reorder(self):
        """Test reordering using a simple variable switch"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        probabilities = np.array([0.1, 0.2, 0.3, 0.4])

        probabilities = reorder_probabilities(probabilities, [a, b], [b, a])
        expected = np.array([0.1, 0.3, 0.2, 0.4])

        np.testing.assert_array_almost_equal(probabilities, expected)
