import unittest

import numpy as np

from bayesnet.core import DiscreteRandomVariable
from bayesnet.testing import ProbabilityMassFunction
from bayesnet.testing import ProbabilityMassFunctionMarginal
from bayesnet.testing import ProbabilityMassFunctionProduct


class TestProbabilityMassFunction(unittest.TestCase):
    """Test the bayesnet.testing.ProbabilityMassFunction class"""

    def test_init(self):
        """Test the __init__ method"""

        a = DiscreteRandomVariable('a')

        # Test simple case with no probabilities.
        pmf = ProbabilityMassFunction([a])
        self.assertIsInstance(pmf, ProbabilityMassFunction)
        self.assertAlmostEqual(pmf.normalization, 1.0)
        self.assertAlmostEqual(pmf.log_normalization, 0.0)
        np.testing.assert_array_almost_equal(
            pmf.probabilities, np.full((2,), 0.5))

        # Test with unnormalized probabilities.
        pmf = ProbabilityMassFunction([a], [3, 1])
        self.assertIsInstance(pmf, ProbabilityMassFunction)
        self.assertAlmostEqual(pmf.normalization, 4.0)
        self.assertAlmostEqual(pmf.log_normalization, np.log(4.0))
        np.testing.assert_array_almost_equal(pmf.probabilities, [0.75, 0.25])

        # Test with a normalization coefficient.
        pmf = ProbabilityMassFunction([a], [0.75, 0.25], 2)
        self.assertIsInstance(pmf, ProbabilityMassFunction)
        self.assertAlmostEqual(pmf.normalization, 2.0)
        self.assertAlmostEqual(pmf.log_normalization, np.log(2.0))
        np.testing.assert_array_almost_equal(pmf.probabilities, [0.75, 0.25])

        # The number of probabilities must match the variables.
        self.assertRaises(
            ValueError, ProbabilityMassFunction, [a], [1.0, 2.0, 3.0])

        # The normalization cannot be provided if the probabilities do not
        # sum to 1.0.
        self.assertRaises(
            ValueError, ProbabilityMassFunction, [a], [1.0, 1.0], 2)


class TestProbabilityMassFunctionMarginal(unittest.TestCase):
    """Test the bayesnet.testing.ProbabilityMassFunctionMarginal class"""

    def test_update(self):
        """Test the update method"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        probabilities = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7]
        pmf = ProbabilityMassFunction([a, b, c], probabilities)

        # The marginal should be updated on initialization.
        marginal = ProbabilityMassFunctionMarginal(pmf, c)
        expected = np.array([0.1, 0.5, 0.9, 1.3]) / 2.8
        np.testing.assert_array_almost_equal(marginal.probabilities, expected)
        self.assertAlmostEqual(marginal.normalization, 2.8)
        self.assertAlmostEqual(marginal.log_normalization, np.log(2.8))

        # Modifying the source PMF and updating should update the
        # probabilities.
        pmf.probabilities = np.full((8,), 1 / 8)
        pmf.normalization = 1.0
        marginal.update()
        expected = np.array([1/4] * 4)
        np.testing.assert_array_almost_equal(marginal.probabilities, expected)
        self.assertAlmostEqual(marginal.normalization, 1.0)
        self.assertAlmostEqual(marginal.log_normalization, 0.0)


class TestProbabilityMassFunctionProduct(unittest.TestCase):
    """Test the bayesnet.testing.ProbabilityMassFunctionProduct class"""

    def test_update(self):
        """Test the update method"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        left = ProbabilityMassFunction([a, b], [0.1, 0.2, 0.3, 0.4])
        right = ProbabilityMassFunction([c, b], [0.4, 0.3, 0.2, 0.1])
        result = ProbabilityMassFunctionProduct(left, right)

        # Verify that the product is updated on initialization.
        expected_probabilities = np.array(
            [0.04, 0.02, 0.06, 0.02, 0.12, 0.06, 0.12, 0.04])
        expected_probabilities /= np.sum(expected_probabilities)
        np.testing.assert_array_almost_equal(
            result.probabilities, expected_probabilities)
        self.assertAlmostEqual(result.normalization, 0.48)
        self.assertAlmostEqual(result.log_normalization, np.log(0.48))

        # Changing the source PMFs and updating should update the product.
        left.probabilities = [0.25, 0.25, 0.25, 0.25]
        result.update()
        expected_probabilities = np.array(
            [0.4, 0.2, 0.3, 0.1, 0.4, 0.2, 0.3, 0.1])
        expected_probabilities /= np.sum(expected_probabilities)
        np.testing.assert_array_almost_equal(
            result.probabilities, expected_probabilities)
        self.assertAlmostEqual(result.normalization, 0.5)
        self.assertAlmostEqual(result.log_normalization, np.log(0.5))
