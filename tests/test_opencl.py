import unittest

import numpy as np

from bayesnet import DiscreteRandomVariable
from bayesnet.opencl import ProbabilityMassFunction
from bayesnet.opencl import ProbabilityMassFunctionProduct
from bayesnet.opencl import ProbabilityMassFunctionMarginal


class TestProbabilityMassFunction(unittest.TestCase):
    """Test the bayesnet.opencl.ProbabilityMassFunction class"""

    def test_init(self):
        """Test the __init__ method and properties"""

        a = DiscreteRandomVariable('a')
        pmf = ProbabilityMassFunction([a], [0.1, 0.9])
        np.testing.assert_array_almost_equal(pmf.probabilities, [0.1, 0.9])


class TestProbabilityMassFunctionMarginal(unittest.TestCase):
    """Test the bayesnet.opencl.ProbabilityMassFunctionMarginal class"""

    def test_init(self):
        """Test the __init__ method and properties"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        pmf = ProbabilityMassFunction([a, b], [0.1, 0.2, 0.3, 0.4])

        marginal = ProbabilityMassFunctionMarginal(pmf, b)
        np.testing.assert_array_almost_equal(
            marginal.probabilities, [0.3, 0.7])

        marginal = ProbabilityMassFunctionMarginal(pmf, a)
        np.testing.assert_array_almost_equal(
            marginal.probabilities, [0.4, 0.6])

    def test_tree_variables(self):
        """Test the marginal when there are 3 variables"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        probabilities = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
        pmf = ProbabilityMassFunction([a, b, c], probabilities)

        marginal = ProbabilityMassFunctionMarginal(pmf, c)
        expected = np.array([0.3, 0.7, 1.1, 1.5])
        np.testing.assert_array_almost_equal(marginal.probabilities, expected)
        self.assertAlmostEqual(marginal.normalization, 1.0)

        marginal = ProbabilityMassFunctionMarginal(pmf, b)
        np.testing.assert_array_almost_equal(
            marginal.probabilities, [0.4, 0.6, 1.2, 1.4])
        self.assertAlmostEqual(marginal.normalization, 1.0)

        marginal = ProbabilityMassFunctionMarginal(pmf, a)
        np.testing.assert_array_almost_equal(
            marginal.probabilities, [0.6, 0.8, 1.0, 1.2])
        self.assertAlmostEqual(marginal.normalization, 1.0)
        self.assertAlmostEqual(marginal.log_normalization, 0.0, 5)


class TestProbabilityMassFunctionProduct(unittest.TestCase):
    """Test the bayesnet.opencl.ProbabilityMassFunctionProduct class"""

    def test_init(self):
        """Test the __init__ method and properties"""

        a = DiscreteRandomVariable('a')
        pmf_a = ProbabilityMassFunction([a], [0.1, 0.9])
        b = DiscreteRandomVariable('b')
        pmf_b = ProbabilityMassFunction([b], [0.1, 0.9])

        product = ProbabilityMassFunctionProduct(pmf_a, pmf_b)

        np.testing.assert_array_almost_equal(
            product.probabilities, [0.01, 0.09, 0.09, 0.81])
        self.assertAlmostEqual(product.normalization, 1.0, 5)
        self.assertAlmostEqual(product.log_normalization, 0.0, 5)

    def test_tree_variables(self):
        """Test the product when there are 3 variables"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        left = ProbabilityMassFunction([a, b], [0.1, 0.2, 0.3, 0.4])
        right = ProbabilityMassFunction([b, c], [0.4, 0.3, 0.2, 0.1])

        product = ProbabilityMassFunctionProduct(left, right)

        expected = np.array([0.04, 0.03, 0.04, 0.02, 0.12, 0.09, 0.08, 0.04])
        expected /= expected.sum()
        np.testing.assert_array_almost_equal(product.probabilities, expected)
        self.assertAlmostEqual(product.normalization, 0.46, 5)
        self.assertAlmostEqual(product.log_normalization, np.log(0.46), 5)
