import unittest

from bayesnet import DiscreteRandomVariable


class TestDiscreteRandomVariable(unittest.TestCase):
    """Test the bn.DiscreteRandomVariable class."""

    def test_init(self):
        """Test the __init__ method."""

        # Create a variable with only a symbol it should have 2 outcomes.
        variable = DiscreteRandomVariable('a')
        self.assertEqual(len(variable), 2)
        self.assertTupleEqual(variable.domain, (0, 1))

        # Create a variable with 3 outcomes.
        variable = DiscreteRandomVariable('b', [0, 1, 2])
        self.assertEqual(len(variable), 3)
        self.assertTupleEqual(variable.domain, (0, 1, 2))

        # The domain cannot contain duplicates.
        self.assertRaises(ValueError, DiscreteRandomVariable, 'c', (0, 1, 1))
