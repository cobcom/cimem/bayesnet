import unittest

import numpy as np

from bayesnet import DiscreteRandomVariable
from bayesnet.core import OrderedSet
from bayesnet.base import ProbabilityMassFunction
from bayesnet.base import ProbabilityMassFunctionMarginal
from bayesnet.base import ProbabilityMassFunctionProduct
from bayesnet.utils import map_subdomain
from bayesnet.testing import ProbabilityMassFunctionMarginal as Marginal
from bayesnet.testing import ProbabilityMassFunctionProduct as ReferenceProduct


class TestMap(unittest.TestCase):
    """Test the map_subdomain function"""

    def test_map_BCA_ABC(self):

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        domain = OrderedSet([a, b, c])
        subdomain = OrderedSet([b, c, a])

        indices = map_subdomain(subdomain, domain)

        np.testing.assert_array_equal(
            indices,
            [0, 2, 4, 6, 1, 3, 5, 7])

    def test_map_AB_ABC(self):

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        domain = OrderedSet([a, b, c])
        subdomain = OrderedSet([a, b])

        indices = map_subdomain(subdomain, domain)

        np.testing.assert_array_equal(
            indices,
            [0, 0, 1, 1, 2, 2, 3, 3])

    def test_map_BA_ABC(self):

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        domain = OrderedSet([a, b, c])
        subdomain = OrderedSet([b, a])

        indices = map_subdomain(subdomain, domain)

        np.testing.assert_array_equal(
            indices,
            [0, 0, 2, 2, 1, 1, 3, 3])

    def test_map_AB_ACB(self):

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        domain = OrderedSet([a, c, b])
        subdomain = OrderedSet([a, b])

        indices = map_subdomain(subdomain, domain)

        np.testing.assert_array_equal(
            indices,
            [0, 1, 0, 1, 2, 3, 2, 3])

    def test_map_AB_BAC(self):

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        domain = OrderedSet([b, a, c])
        subdomain = OrderedSet([a, b])

        indices = map_subdomain(subdomain, domain)

        np.testing.assert_array_equal(
            indices,
            [0, 0, 2, 2, 1, 1, 3, 3])


class TestProbabilityMassFunction(unittest.TestCase):
    """Test the bn.pmf.ProbabilityMassFunction class."""

    def test_init(self):
        """Test the __init__ method."""

        variables = [
            DiscreteRandomVariable('a'),
            DiscreteRandomVariable('b', (0, 1, 2))
        ]

        probabilities = [0.1, 0.2, 0.3, 0.35, 0.0, 0.05]

        pmf = ProbabilityMassFunction(variables, probabilities,
                                      normalization=2.0)
        self.assertEqual(pmf.size, 6)
        self.assertEqual(len(pmf.variables), 2)
        self.assertEqual(pmf.nb_variables, 2)
        np.testing.assert_array_almost_equal(pmf.probabilities, probabilities)
        np.testing.assert_array_almost_equal(pmf.unnormalized,
                                             np.array(probabilities) * 2.0)

        # The variables must all be discrete random variables.
        wrong_variables = (variables[0], None)
        self.assertRaises(TypeError, ProbabilityMassFunction,
                          wrong_variables, probabilities)

        # The number of probabilities must match the number of variables
        # states.
        wrong_probabilities = probabilities[:3]
        self.assertRaises(ValueError, ProbabilityMassFunction,
                          variables, wrong_probabilities)

        # The probabilities cannot all be 0.
        wrong_probabilities = np.zeros_like(probabilities)
        self.assertRaises(ValueError, ProbabilityMassFunction,
                          variables, wrong_probabilities)

        # Cannot supply a normalization coefficient of 0.
        wrong_normalization = 0.0
        self.assertRaises(ValueError, ProbabilityMassFunction,
                          variables, probabilities,
                          normalization=wrong_normalization)

        # Cannot supply a normalization coefficient with unnormalized
        # probabilities.
        wrong_probabilities = np.ones_like(probabilities)
        self.assertRaises(ValueError, ProbabilityMassFunction,
                          variables, wrong_probabilities,
                          normalization=2.0)

    def test_contains(self):
        """Test the __contains__ method."""

        variables = [
            DiscreteRandomVariable('a'),
            DiscreteRandomVariable('b', (0, 1, 2))
        ]

        probabilities = np.random.rand(6)

        pmf = ProbabilityMassFunction(variables, probabilities)
        self.assertTrue(variables[0] in pmf)
        self.assertTrue(variables[1] in pmf)
        self.assertFalse(DiscreteRandomVariable('c') in pmf)

    def test_eq(self):
        """Test the __eq__ method."""

        variables = [
            DiscreteRandomVariable('a'),
            DiscreteRandomVariable('b'),
        ]

        probabilities = np.random.rand(4)

        pmf1 = ProbabilityMassFunction(variables, probabilities)
        pmf2 = ProbabilityMassFunction(variables, probabilities * 2.0)
        self.assertEqual(pmf1, pmf2)

        # The order of the variables does not matter.
        probabilities = probabilities[[0, 2, 1, 3]]
        pmf3 = ProbabilityMassFunction(variables[::-1], probabilities)
        self.assertEqual(pmf1, pmf3)

        # PMFs with different variables are not equal.
        pmf1 = ProbabilityMassFunction(variables, probabilities)
        pmf2 = ProbabilityMassFunction(variables[:1], probabilities[:2])
        self.assertNotEqual(pmf1, pmf2)

        # PMFs with different probabilities are not equal.
        pmf1 = ProbabilityMassFunction(variables, probabilities)
        pmf2 = ProbabilityMassFunction(variables, probabilities[::-1])
        self.assertNotEqual(pmf1, pmf2)

    def test_events(self):
        """Test the events property."""

        variables = [
            DiscreteRandomVariable('a'),
            DiscreteRandomVariable('b', (0, 1, 2)),
        ]
        probabilities = np.random.rand(6)
        pmf = ProbabilityMassFunction(variables, probabilities)

        events = list(pmf.events)
        self.assertSequenceEqual(events, [(0, 0), (0, 1), (0, 2),
                                          (1, 0), (1, 1), (1, 2)])

    def test_repr(self):
        """Test the __repr__ method."""

        # Just verify that we get a string.
        pmf = ProbabilityMassFunction([DiscreteRandomVariable('a')], [0, 1])
        self.assertTrue(isinstance(repr(pmf), str))

    def test_str(self):
        """Test the __repr__ method."""

        # Just verify that we get a string.
        pmf = ProbabilityMassFunction([DiscreteRandomVariable('a')], [0, 1])
        self.assertTrue(isinstance(str(pmf), str))


class TestOrderedSet(unittest.TestCase):
    """Test the bn.OrderedSet class."""

    def test_and(self):
        """Test the __and__ method"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        left = OrderedSet((a, b, c))
        right = OrderedSet((c, a))
        intersection = left & right
        self.assertTrue(intersection == OrderedSet((a, c)))

    def test_new(self):
        """Test the __new__ method"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        # Create a simple domain.
        self.assertTrue(isinstance(OrderedSet((a, b, c)), OrderedSet))

        # Duplicates should raise an error.
        self.assertRaises(ValueError, OrderedSet, (a, a, b))

    def test_eq(self):
        """Test the __eq__ method"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        self.assertTrue(OrderedSet((a, b)) == OrderedSet((a, b)))
        self.assertTrue(OrderedSet((a, b)) == OrderedSet((b, a)))

        self.assertFalse(OrderedSet((a,)) == OrderedSet((a, b)))
        self.assertFalse(OrderedSet((a, b)) == OrderedSet((c, a)))
        self.assertFalse(OrderedSet((a, b, c)) == OrderedSet((a, b)))

    def test_ge(self):
        """Test the __ge__ method"""

        # A domain is greater than or equal to another if its variables
        # are a superset of the other.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        self.assertTrue(OrderedSet((a, b, c)) >= OrderedSet((a, b)))
        self.assertTrue(OrderedSet((a, b, c)) >= OrderedSet((c, a)))
        self.assertTrue(OrderedSet((a, b, c)) >= OrderedSet((b,)))
        self.assertTrue(OrderedSet((a, b)) >= OrderedSet((a, b)))

        self.assertFalse(OrderedSet((a, b)) >= OrderedSet((c,)))
        self.assertFalse(OrderedSet((a, b)) >= OrderedSet((a, c)))

    def test_gt(self):
        """Test the __gt__ method"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        self.assertTrue(OrderedSet((a, b, c)) > OrderedSet((a, b)))
        self.assertTrue(OrderedSet((a, b, c)) > OrderedSet((c, a)))
        self.assertTrue(OrderedSet((a, b, c)) > OrderedSet((b,)))

        self.assertFalse(OrderedSet((a, b)) > OrderedSet((a, b)))
        self.assertFalse(OrderedSet((a, b)) > OrderedSet((c,)))
        self.assertFalse(OrderedSet((a, b)) > OrderedSet((a, c)))

    def test_le(self):
        """Test the __le__ method"""

        # A domain is less than or equal to another if its variables
        # are a subset of the other.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        self.assertTrue(OrderedSet((a, b)) <= OrderedSet((a, b, c)))
        self.assertTrue(OrderedSet((c, a)) <= OrderedSet((a, b, c)))
        self.assertTrue(OrderedSet((b,)) <= OrderedSet((a, b, c)))
        self.assertTrue(OrderedSet((a, b)) <= OrderedSet((a, b)))

        self.assertFalse(OrderedSet((c,)) <= OrderedSet((a, b)))
        self.assertFalse(OrderedSet((a, c)) <= OrderedSet((a, b)))

    def test_lt(self):
        """Test the __lt__ method"""

        # A domain is less than another if its variables are a subset of
        # the other.
        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        self.assertTrue(OrderedSet((a, b)) < OrderedSet((a, b, c)))
        self.assertTrue(OrderedSet((c, a)) < OrderedSet((a, b, c)))
        self.assertTrue(OrderedSet((b,)) < OrderedSet((a, b, c)))

        self.assertFalse(OrderedSet((a, b)) < OrderedSet((a, b)))
        self.assertFalse(OrderedSet((c,)) < OrderedSet((a, b)))
        self.assertFalse(OrderedSet((a, c)) < OrderedSet((a, b)))

    def test_or(self):
        """Test the __or__ method"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')
        d = DiscreteRandomVariable('d')
        e = DiscreteRandomVariable('e')

        # The new domain is the union of the inputs.
        left = OrderedSet((a, b, c))
        right = OrderedSet((d, e))
        self.assertEqual(left | right, OrderedSet((a, b, c, d, e)))

        # Duplicates should be removed.
        left = OrderedSet((a, b, c))
        right = OrderedSet((c, d, e))
        self.assertEqual(left | right, OrderedSet((a, b, c, d, e)))

    def test_ne(self):
        """Test the __ne__ method"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        self.assertTrue(OrderedSet((a,)) != OrderedSet((a, b)))
        self.assertTrue(OrderedSet((a, b)) != OrderedSet((c, a)))
        self.assertTrue(OrderedSet((a, b, c)) != OrderedSet((a, b)))

        self.assertFalse(OrderedSet((a, b)) != OrderedSet((a, b)))
        self.assertFalse(OrderedSet((a, b)) != OrderedSet((b, a)))

    def test_sub(self):
        """Test the __sub__ method"""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        # If the item is present, it is removed.
        some_set = OrderedSet((a, b, c))
        some_other_set = some_set - b
        self.assertEqual(some_other_set, OrderedSet((a, c)))

        # Cannot remove what is not present.
        some_set = OrderedSet((a, c))

        def remove_2():
            some_set - b

        self.assertRaises(ValueError, remove_2)


class TestProbabilityMassFunctionMarginal(unittest.TestCase):
    """Test the ProbabilityMassFunctionMarginal class"""

    def test_init(self):
        """Test the __init__ method."""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        probabilities = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7]
        pmf = ProbabilityMassFunction([a, b, c], probabilities)

        # Computing the marginal using the * or the class should be the
        # same.
        expected_marginal = Marginal(pmf, c)
        marginal = ProbabilityMassFunctionMarginal(pmf, c)
        self.assertEqual(expected_marginal, marginal)

        # Cannot marginalize a variable that is not in the PMF.
        d = DiscreteRandomVariable('d')
        self.assertRaises(ValueError, ProbabilityMassFunctionMarginal, pmf, d)

    def test_recur(self):
        """Test the __recur__ method."""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')

        probabilities = [0.1, 0.2, 0.3, 0.4]
        pmf = ProbabilityMassFunction([a, b], probabilities)

        # Iterating over the marginal should yield the source PMF.
        marginal = ProbabilityMassFunctionMarginal(pmf, b)
        source = [pmf for pmf in marginal]
        self.assertListEqual(source, [marginal, pmf])
        source = [pmf for pmf in reversed(marginal)]
        self.assertListEqual(source, [pmf, marginal])


class TestProbabilityMassFunctionProduct(unittest.TestCase):
    """Test the ProbabilityMassFunctionProduct class"""

    def test_init(self):
        """Test the __init__ method."""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')
        c = DiscreteRandomVariable('c')

        probabilities = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7]
        left = ProbabilityMassFunction([a, b, c], probabilities)
        probabilities = [0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0]
        right = ProbabilityMassFunction([c, b, a], probabilities)

        # Computing the product using the class should be the
        # same as the reference class.
        expected_product = ReferenceProduct(left, right)
        product = ProbabilityMassFunctionProduct(left, right)
        self.assertEqual(expected_product, product)

    def test_recur(self):
        """Test the __recur__ method."""

        a = DiscreteRandomVariable('a')
        b = DiscreteRandomVariable('b')

        probabilities = [0.0, 0.1, 0.2, 0.3]
        left = ProbabilityMassFunction([a, b], probabilities)
        probabilities = [0.7, 0.6, 0.5, 0.4]
        right = ProbabilityMassFunction([b, a], probabilities)

        product = ProbabilityMassFunctionProduct(left, right)

        # Iterating over the product should yield the source PMFs.
        source = [pmf for pmf in product]
        self.assertListEqual(source, [product, left, right])
        source = [pmf for pmf in reversed(product)]
        self.assertListEqual(source, [left, right, product])
