import unittest
from functools import reduce

from numpy.testing import assert_array_almost_equal

from bayesnet.core import DiscreteRandomVariable
from bayesnet.base import ProbabilityMassFunction
from bayesnet.junction import construct_junction_trees
from bayesnet.junction import _collect, _distribute, _marginals
from bayesnet.junction import Marginals
from bayesnet.opencl import ProbabilityMassFunction as \
    CLProbabilityMassFunction
from bayesnet.opencl import ProbabilityMassFunctionProduct as CLProduct
from bayesnet.opencl import ProbabilityMassFunctionMarginal as CLMarginal
from bayesnet.base import ProbabilityMassFunctionProduct
from bayesnet.base import ProbabilityMassFunctionMarginal
from bayesnet.testing import ProbabilityMassFunctionMarginal as Marginal
from bayesnet.testing import ProbabilityMassFunctionProduct as Product


def car_start_problem():
    """Generate the tables for the car start problem."""

    # The variables of problem:
    #   fuel  : Does the car have fuel?
    #   meter : What does the fuel meter say?
    #   start : Does the car start?
    #   spark : Are the spark plugs clean?
    fuel = DiscreteRandomVariable('Fu', (0, 1))
    meter = DiscreteRandomVariable('Fm', (0, 1, 2))
    start = DiscreteRandomVariable('St', (0, 1))
    spark = DiscreteRandomVariable('Sp', (0, 1))

    # It is very likely the car has fuel.
    fuel_pmf = ProbabilityMassFunction([fuel], [0.02, 0.98])

    # It is very likely the spark plug is clean.
    spark_pmf = ProbabilityMassFunction([spark], [0.04, 0.96])

    # The meter depends on the fuel.
    meter_pmf = ProbabilityMassFunction(
        [fuel, meter], [0.998, 0.001, 0.001, 0.01, 0.60, 0.39])

    # The car will start if there is fuel and the spark plug is clean.
    start_table = ProbabilityMassFunction(
        [start, fuel, spark], [1.0, 1.0, 0.99, 0.01, 0.0, 0.0, 0.01, 0.99])

    variables = fuel, meter, start, spark
    tables = fuel_pmf, spark_pmf, meter_pmf, start_table

    return variables, tables


def temporal_clusters_problem(pmf_class=ProbabilityMassFunction):
    """Generates the tables for the temporal clusters problem"""

    # There are 2 clusters at 2 time samples.
    clusters = [DiscreteRandomVariable(str(s)) for s in range(4)]

    # Add temporal constraints between clusters.
    pmfs = [pmf_class(clusters[0:2], [0.49, 0.01, 0.01, 0.49])]
    pmfs += [pmf_class(clusters[2:], [0.49, 0.01, 0.01, 0.49])]
    pmfs += [pmf_class([clusters[0], clusters[3]],
                                     [0.01, 0.49, 0.49, 0.01])]

    pmfs += [pmf_class(clusters[i:i + 1], [0.5, 0.5]) for i in range(4)]

    return clusters, pmfs


class TestBasicUsingCarStartProblem(unittest.TestCase):
    """Tests using the car start problem"""

    def test_car_no_start(self):
        """Test the case where the car does not start."""

        # Get the variables and tables of the network.
        variables, tables = car_start_problem()
        fuel, meter, start, spark = variables

        # Add evidence: the car does not start.
        evidence_start = ProbabilityMassFunction([start], [1.0, 0.0])
        no_start_tables = tables + (evidence_start,)
        full_pmf = reduce(Product, no_start_tables)

        # Verify the result agree with the reference.
        fuel_marginal = Marginal(
            Marginal(Marginal(full_pmf, meter), start), spark)
        expected_fuel_marginal = ProbabilityMassFunction(
            [fuel], [0.2931863492, 0.7068136507])
        self.assertEqual(fuel_marginal, expected_fuel_marginal)
        spark_marginal = Marginal(
            Marginal(Marginal(full_pmf, fuel), meter), start)
        expected_spark_marginal = ProbabilityMassFunction(
            [spark], [0.5806242460, 0.4193737539])
        self.assertEqual(spark_marginal, expected_spark_marginal)

    def test_car_no_start_half_fuel(self):
        """Test the case where the car does not start with fuel."""

        # Get the variables and tables of the network.
        variables, tables = car_start_problem()
        fuel, meter, start, spark = variables

        # Add evidence: the car does not start.
        evidence_start = ProbabilityMassFunction([start], [1.0, 0.0])
        no_start_tables = tables + (evidence_start,)

        # Add evidence: the car has fuel.
        evidence_meter = ProbabilityMassFunction([meter], [0.0, 1.0, 0.0])
        half_full_no_start_tables = no_start_tables + (evidence_meter,)
        full_pmf = reduce(Product, half_full_no_start_tables)

        # Verify the result agree with the reference.
        spark_marginal = Marginal(
            Marginal(Marginal(full_pmf, fuel), meter), start)
        expected_spark_marginal = ProbabilityMassFunction(
            [spark], [0.8043496283, 0.1956503716])
        self.assertEqual(spark_marginal, expected_spark_marginal)
        fuel_marginal = Marginal(
            Marginal(Marginal(full_pmf, meter), start), spark)
        expected_fuel_marginal = ProbabilityMassFunction(
            [fuel], [0.0006908558, 0.9993091441])
        self.assertEqual(fuel_marginal, expected_fuel_marginal)


class TestReferenceUsingTemporalClusterProblem(unittest.TestCase):
    """Tests the reference implementation using the clusters problem"""

    def test_marginals(self):
        """Test the marginals of all variables"""

        # Compute the marginal for each cluster.
        variables, pmfs = temporal_clusters_problem()
        marginals = Marginals(pmfs, Product, Marginal)

        for v in variables:
            self.assertAlmostEqual(marginals[v].normalization, 0.25 / 16)
            probabilities = marginals[v].probabilities
            assert_array_almost_equal(probabilities, [0.5, 0.5])

    def test_marginals_with_evidence(self):
        """Test the marginals when adding evidence"""

        variables, pmfs = temporal_clusters_problem()
        pmfs += [ProbabilityMassFunction(variables[0:1], [1.0, 0.0])]
        marginals = Marginals(pmfs, Product, Marginal)

        for v in variables:
            self.assertAlmostEqual(marginals[v].normalization, 0.125 / 16)
        probabilities = marginals[variables[0]].probabilities
        assert_array_almost_equal(probabilities, [1.0, 0.0])
        probabilities = marginals[variables[1]].probabilities
        assert_array_almost_equal(probabilities, [0.98, 0.02])
        probabilities = marginals[variables[2]].probabilities
        assert_array_almost_equal(probabilities, [0.0392, 0.9608])
        probabilities = marginals[variables[3]].probabilities
        assert_array_almost_equal(probabilities, [0.02, 0.98])

        # The normalization must be the same for all marginals.
        for variable in variables:
            self.assertAlmostEqual(marginals[variable].normalization,
                                   marginals[variables[0]].normalization)

        # This should also be true after an update.
        pmfs[-1].normalization = 22.0
        marginals.update()
        for variable in variables:
            self.assertAlmostEqual(marginals[variable].normalization,
                                   marginals[variables[0]].normalization)


class TestPrecomputedUsingCarStartProblem(unittest.TestCase):
    """Tests precomputed PMFs using the car start problem"""

    def test_car_no_start(self):
        """Test the case where the car does not start."""

        # Get the variables and tables of the network.
        variables, tables = car_start_problem()
        fuel, meter, start, spark = variables

        # Add evidence: the car does not start.
        evidence_start = ProbabilityMassFunction([start], [1.0, 0.0])
        no_start_tables = tables + (evidence_start,)
        full_pmf = reduce(ProbabilityMassFunctionProduct, no_start_tables)

        # Verify the result agree with the reference.
        fuel_marginal = ProbabilityMassFunctionMarginal(
            ProbabilityMassFunctionMarginal(
                ProbabilityMassFunctionMarginal(full_pmf, meter),
                start
            ),
            spark
        )
        expected_fuel_marginal = ProbabilityMassFunction(
            [fuel], [0.2931863492, 0.7068136507])
        self.assertEqual(fuel_marginal, expected_fuel_marginal)

        spark_marginal = ProbabilityMassFunctionMarginal(
            ProbabilityMassFunctionMarginal(
                ProbabilityMassFunctionMarginal(full_pmf, fuel),
                meter,
            ),
            start
        )
        expected_spark_marginal = ProbabilityMassFunction(
            [spark], [0.5806242460, 0.4193737539])
        self.assertEqual(spark_marginal, expected_spark_marginal)

    def test_car_no_start_half_fuel(self):
        """Test the case where the car does not start with fuel."""

        # Get the variables and tables of the network.
        variables, tables = car_start_problem()
        fuel, meter, start, spark = variables

        # Add evidence: the car does not start.
        evidence_start = ProbabilityMassFunction([start], [1.0, 0.0])
        no_start_tables = tables + (evidence_start,)

        # Add evidence: the car has fuel.
        evidence_meter = ProbabilityMassFunction([meter], [0.0, 1.0, 0.0])
        half_full_no_start_tables = no_start_tables + (evidence_meter,)
        full_pmf = reduce(ProbabilityMassFunctionProduct,
                          half_full_no_start_tables)

        # Verify the result agree with the reference.
        spark_marginal = ProbabilityMassFunctionMarginal(
            ProbabilityMassFunctionMarginal(
                ProbabilityMassFunctionMarginal(full_pmf, start),
                meter),
            fuel)
        expected_spark_marginal = ProbabilityMassFunction(
            [spark], [0.8043496283, 0.1956503716])
        self.assertEqual(spark_marginal, expected_spark_marginal)

        fuel_marginal = ProbabilityMassFunctionMarginal(
            ProbabilityMassFunctionMarginal(
                ProbabilityMassFunctionMarginal(full_pmf, spark),
                start),
            meter)
        expected_fuel_marginal = ProbabilityMassFunction(
            [fuel], [0.0006908558, 0.9993091441])
        self.assertEqual(fuel_marginal, expected_fuel_marginal)


class TestJunctionUsingTemporalClusterProblem(unittest.TestCase):
    """Test the junction tree algorithm using the cluster problem"""

    def test_marginals(self):
        """Test the marginals using different implementations"""

        variables, pmfs = temporal_clusters_problem(CLProbabilityMassFunction)

        # Compute the marginals using different implementations.
        reference_marginals = Marginals(pmfs, Product, Marginal)
        pmf_marginals = Marginals(
            pmfs, ProbabilityMassFunctionProduct,
            ProbabilityMassFunctionMarginal)
        opencl_marginals = Marginals(pmfs, CLProduct, CLMarginal)

        # The result should be same no matter the implementation.
        for variable in variables:
            self.assertEqual(reference_marginals[variable],
                             pmf_marginals[variable])
            self.assertEqual(reference_marginals[variable],
                             opencl_marginals[variable])


class TestJunctionUsingCarStartProblem(unittest.TestCase):
    """Test the junction tree algorithm using the car start problem"""

    def test_car_no_start(self):
        """Test the case where the car does not start."""

        # Get the variables and tables of the network.
        variables, tables = car_start_problem()
        fuel, meter, start, spark = variables

        # Add evidence: the car does not start.
        evidence_start = ProbabilityMassFunction([start], [1.0, 0.0])
        no_start_tables = tables + (evidence_start,)

        # Compute the marginal for the spark plug and fuel.
        junction_trees = construct_junction_trees(no_start_tables)
        self.assertEqual(len(junction_trees), 1)
        junction_tree = junction_trees[0]
        update_list = _collect(junction_tree)
        update_list += _distribute(junction_tree)
        marginals, marginals_update_list = _marginals(junction_tree)
        update_list += marginals_update_list
        marginal_variables = [m.variables[0] for m in marginals]

        self.assertEqual(len(marginals), 4)

        # Verify the result agree with the reference.
        fuel_marginal = marginals[marginal_variables.index(variables[0])]
        expected_fuel_marginal = ProbabilityMassFunction(
            [fuel], [0.2931863492, 0.7068136507])
        self.assertEqual(fuel_marginal, expected_fuel_marginal)

        spark_marginal = marginals[marginal_variables.index(variables[3])]
        expected_spark_marginal = ProbabilityMassFunction(
            [spark], [0.5806242460, 0.4193737539])
        self.assertEqual(spark_marginal, expected_spark_marginal)

    def test_car_no_start_half_fuel(self):
        """Test the case where the car does not start with fuel"""

        # Get the variables and tables of the network.
        variables, tables = car_start_problem()
        fuel, meter, start, spark = variables

        # Add evidence: the car does not start.
        evidence_start = ProbabilityMassFunction([start], [1.0, 0.0])
        no_start_tables = tables + (evidence_start,)

        # Add evidence: the car has fuel.
        evidence_meter = ProbabilityMassFunction([meter], [0.0, 1.0, 0.0])
        half_full_no_start_tables = no_start_tables + (evidence_meter,)

        # Compute the marginal for the spark plug and fuel.
        junction_trees = construct_junction_trees(half_full_no_start_tables)
        self.assertEqual(len(junction_trees), 1)
        junction_tree = junction_trees[0]
        update_list = _collect(junction_tree)
        update_list += _distribute(junction_tree)
        marginals, marginals_update_list = _marginals(junction_tree)
        update_list += marginals_update_list
        marginal_variables = [m.variables[0] for m in marginals]

        self.assertEqual(len(marginals), 4)

        # Verify the result agree with the reference.
        spark_marginal = marginals[marginal_variables.index(variables[3])]
        expected_spark_marginal = ProbabilityMassFunction(
            [spark], [0.8043496283, 0.1956503716])
        self.assertEqual(spark_marginal, expected_spark_marginal)

        fuel_marginal = marginals[marginal_variables.index(variables[0])]
        expected_fuel_marginal = ProbabilityMassFunction(
            [fuel], [0.0006908558, 0.9993091441])
        self.assertEqual(fuel_marginal, expected_fuel_marginal)


class TestMarginalsUsingCarStartProblem(unittest.TestCase):
    """Test the Marginals class using the car start problem"""

    def test_car_no_start(self):
        """Test the case where the car does not start."""

        # Get the variables and tables of the network.
        variables, tables = car_start_problem()
        fuel, meter, start, spark = variables

        # Add evidence: the car does not start.
        evidence_start = ProbabilityMassFunction([start], [1.0, 0.0])
        no_start_tables = tables + (evidence_start,)

        # Compute all the marginals.
        marginals = Marginals(no_start_tables)

        # Verify the result agree with the reference.
        fuel_marginal = marginals[variables[0]]
        expected_fuel_marginal = ProbabilityMassFunction(
            [fuel], [0.2931863492, 0.7068136507])
        self.assertEqual(fuel_marginal, expected_fuel_marginal)

        spark_marginal = marginals[variables[3]]
        expected_spark_marginal = ProbabilityMassFunction(
            [spark], [0.5806242460, 0.4193737539])
        self.assertEqual(spark_marginal, expected_spark_marginal)

    def test_car_no_start_half_fuel(self):
        """Test the case where the car does not start with fuel"""

        # Get the variables and tables of the network.
        variables, tables = car_start_problem()
        fuel, meter, start, spark = variables

        # Add evidence: the car does not start.
        evidence_start = ProbabilityMassFunction([start], [1.0, 0.0])
        no_start_tables = tables + (evidence_start,)

        # Add evidence: the car has fuel.
        evidence_meter = ProbabilityMassFunction([meter], [0.0, 1.0, 0.0])
        half_full_no_start_tables = no_start_tables + (evidence_meter,)

        # Compute all the marginals.
        marginals = Marginals(half_full_no_start_tables)

        # Verify the result agree with the reference.
        spark_marginal = marginals[variables[3]]
        expected_spark_marginal = ProbabilityMassFunction(
            [spark], [0.8043496283, 0.1956503716])
        self.assertEqual(spark_marginal, expected_spark_marginal)

        fuel_marginal = marginals[variables[0]]
        expected_fuel_marginal = ProbabilityMassFunction(
            [fuel], [0.0006908558, 0.9993091441])
        self.assertEqual(fuel_marginal, expected_fuel_marginal)
