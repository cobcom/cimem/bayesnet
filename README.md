# bayesnet
A Python package to repeatedly obtain the marginal distributions of a Bayesian
network using exact inference.

## Installation

For now, most people are expected to use `bayesnet` as developers and should
 install it by moving into the root `bayesnet` directory and running:
```bash
python setup.py develop
```
To make sure `bayesnet` was installed correctly, test it by running:
```bash
python -m unittest
```

## Documentation

Coming soon!

## Quickstart

Coming soon!
